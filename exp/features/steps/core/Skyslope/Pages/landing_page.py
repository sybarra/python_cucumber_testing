# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from skyslope.pages.skyslope_base import BasePage

class LandingPage(BasePage):
    def __init__(self, selenium_driver):
        BasePage.__init__(
                self,
                selenium_driver
        )
        
    locator_dictionary = {
        "login_button": (By.LINK_TEXT, "Login")
    }
