from selenium.webdriver.common.by import By
from skyslope.pages.skyslope_base import BasePage

class TopMenu(BasePage):
    def __init__(self, selenium_driver):
        BasePage.__init__(self, selenium_driver)

    locator_dictionary = {
        "homelogo": (By.ID, "homeLogo"),
        "logo_dropdown": (By.XPATH, "//button/div/span[text()='Data 1110 Feed ']"),
        "logout": (By.LINK_TEXT, "Log Out")
    }

    def skyslope_logout(self):
        self.find_element(
            *self.locator_dictionary["logo_dropdown"]).click()
        self.find_element(
            *self.locator_dictionary["logout"]).click()