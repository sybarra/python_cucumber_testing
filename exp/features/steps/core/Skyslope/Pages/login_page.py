# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from skyslope.pages.skyslope_base import BasePage

class LoginPage(BasePage):
    def __init__(self, selenium_driver):
        BasePage.__init__(self, selenium_driver)
        
        
    locator_dictionary = {
            "username": (By.ID, "username"),
            #"username": (By.XPATH, "//input[@placeholder='Email']"),
            "password": (By.ID, "password"),
            "sign_in": (By.XPATH, "//button[text()='Sign in']")
    }
    
    def login(self, username, password):
        self.find_element(*self.locator_dictionary['username']).send_keys(username)
        self.find_element(*self.locator_dictionary['password']).send_keys(password)
        self.find_element(*self.locator_dictionary['sign_in']).click()
