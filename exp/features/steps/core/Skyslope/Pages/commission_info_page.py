from selenium.webdriver.common.by import By
from skyslope.pages.skyslope_base import BasePage

class CommissionInfo(BasePage):
    def __init__(self, selenium_driver):
        BasePage.__init__(self, selenium_driver)

    locator_dictionary = {
        # Buttons
        "submit": (By.LINK_TEXT, "Submit"),
       
        # Fields
        "SalesComm_Percent": (By.ID, "ContentPlaceHolder1_txtSaleCommisionPercentage"),
        "SalesComm_Amount": (By.ID, "ContentPlaceHolder1_txtsalescommissionamt")
    }

    def fill_commission_info(self, sales_comm_amount="5.00"):
        self.fill_text_field(
            self.find_element(*self.locator_dictionary["SalesComm_Percent"]),
            sales_comm_amount
        )