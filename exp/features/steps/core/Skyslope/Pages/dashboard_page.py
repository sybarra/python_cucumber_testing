# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from skyslope.pages.skyslope_base import BasePage

class Dashboard(BasePage):
    def __init__(self, selenium_driver):
        BasePage.__init__(self, selenium_driver)
        
    locator_dictionary = {
        "manage_transactions": (By.XPATH,
                                "//a[contains(@href, 'ManageTransactions')]"),
        "manage_listings": (By.XPATH,
                            "//a[contains(@href, 'ManageListings')]")
    }
        
