# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from skyslope.pages.skyslope_base import BasePage

import time
from datetime import date, timedelta

class CreateTrxMain(BasePage):
    def __init__(self, selenium_driver):
        BasePage.__init__(self, selenium_driver)
        
    locator_dictionary = {
        #misc
        "generic_dropdown": (By.XPATH, "//span[contains(@class, 'dropdown')]/input"),
        "addr_dialog": (By.XPATH, "//div[div/h3='Search for Address']"),
        "addr_dialog_close": (By.XPATH, "//div[h3='Search for Address']/button[@class='close']"),
        
        #buttons
        "next": (By.LINK_TEXT, "Next"),
        "cancel": (By.LINK_TEXT, "Cancel"), 

        #Dropdown Fields
        "Agent": (By.XPATH, "//div[label='Agent']//span[text()='Click to Search']"),
        "Checklist_Type": (By.XPATH, "//div[label='Checklist Type']//span[text()='Select']"),
        "Type_Rep": (By.XPATH, "//div[label='Type (Representation)']//span[text()='Select']"),
        "Source": (By.XPATH, "//div[label='Source']//span[text()='Select']"),
        #TODO: find xpaths for these... this one is WRONG
        "Co-Buyer": (By.XPATH, "//div[label='Co-Buyer Agent']//span[text()='Click to Search']"),
        "Office_Lead": (By.XPATH, "//div[label='Office Lead?']//select"),
        
        #Text Fields
        "MLS": (By.XPATH, "//div[label='MLS#']//input"),
        "Street_No": (By.XPATH, "//div[label='Street No.']//input"),
        "Street_Name": (By.XPATH, "//div[label='Street Name']//input"),
        "Unit": (By.XPATH, "//div[label='Unit #']//input"),
        "ZIP": (By.XPATH, "//div[label='Zip Code']//input"),
        "City": (By.XPATH, "//div[label='City']//input"),
        "State": (By.XPATH, "//div[label='State']//input"),
        "Sale_Price": (By.XPATH, "//div[label='Sale Price']//input"),
        "Year_Built": (By.XPATH, "//div[label='Year Built']//input"),
        "File_ID": (By.XPATH, "//div[label='File ID']//input"),
        "Closing_Date": (By.XPATH, "//div[label='Closing Date']//input"),
        "Acceptance_Date": (By.XPATH, "//div[label='Acceptance Date']//input"),       
    }

    def today_plus(self, numDays):
        plusDate = date.today() + timedelta(days=numDays)
        return plusDate.strftime("%m/%d/%y")
    
    def dismiss_addr_dialog(self):
        close_button = self.find_element(*self.locator_dictionary["addr_dialog_close"])
        time.sleep(1)
        close_button.click()
    
    def fill_text_field(self, loc_name, value):
        """
        desc: clears and writes to a text field.
        param: loc_name (string) key from locator_dictionary
        param: text to be entered into the field.
        """
        field = self.find_element(*self.locator_dictionary[loc_name])
        field.location_once_scrolled_into_view
        field.clear()
        field.send_keys(value)
        
    def fill_text_to_dropdown_field(self, loc_name, value):
        field = self.find_element(*self.locator_dictionary[loc_name])
        field.location_once_scrolled_into_view
        field.click()
        
        dropdown = self.find_element(
                *self.locator_dictionary["generic_dropdown"])
        
        dropdown.click()
        dropdown.send_keys(value)
        
        selection_loc = (By.XPATH, "//li[text()='{}']".format(value))
        selection = self.wait_is_clickable(selection_loc)
        selection.click()

    def fill_office_lead(self, value):
        field = Select(self.find_element(*self.locator_dictionary["Office_Lead"]))
        field.select_by_visible_text(value)

    def fill_fileID(self, value):
        """
        desc: clears and writes to the File ID field.
        param: text to be entered into the field.
        """
        field = self.wait_is_clickable(self.locator_dictionary["File_ID"])
        field.location_once_scrolled_into_view
        field.clear()
        field.click()
        field.send_keys(value)
        
    
    def fill_form(self, agent="App Team", mls=1234, 
                  checklist_type="Traditional Sale", street_num=1,
                  street_name="QA St.", unit=1, zip="86336", state="AZ",
                  city="Sedona", type_rep="Purchase / Tenant",
                  sale_price="120500.00", yr_built="2005", source="Sign",
                  file_id="", office_lead="No", closing_date="01/01/2020",
                  accp_date="01/01/2020"):

        # If not specified, accp_date and closing_date will be updated to 
        # a current date
        if closing_date == "01/01/2020":
            closing_date = self.today_plus(10)
        
        if accp_date == "01/01/2020":
            accp_date = self.today_plus(0)

        self.fill_text_to_dropdown_field("Agent", agent)
        self.fill_text_field("MLS", mls)
        self.fill_text_to_dropdown_field("Checklist_Type", checklist_type)
        self.fill_text_field("Street_No", street_num)
        self.fill_text_field("Street_Name", street_name)
        self.fill_text_field("Unit", unit)
        self.fill_text_field("ZIP", zip)
        self.fill_text_field("State", state)
        self.fill_text_field("City", city)
        self.fill_text_to_dropdown_field("Type_Rep", type_rep)
        self.fill_text_field("Sale_Price", sale_price)
        self.fill_text_field("Year_Built", yr_built)
        self.fill_text_to_dropdown_field("Source", source)
        #TODO: I think the below comment does not apply anymore.
        #Sleep is not preferred but is required here.
        time.sleep(1)
        self.fill_text_field("Acceptance_Date", accp_date)
        self.fill_office_lead(office_lead)
        self.fill_text_field("File_ID", file_id)
        self.fill_text_field("Closing_Date", closing_date)
        
    