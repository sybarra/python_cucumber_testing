# -*- coding: utf-8 -*-
"""
Created on Fri Feb  7 11:59:22 2020

@author: sybarra
"""
import time, csv, traceback
from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import ElementClickInterceptedException

class BasePage(object):
    def __init__(self, selenium_driver, base_url="https://skyslope.com"):
        self.driver = selenium_driver
        self.base_url = base_url
        self.timeout = 30
        self.elem_status = True
        
    # def find_element(self, *loc):
    #     return self.driver.find_element(*loc)

    def __getattr__(self, what, attempts=3):
        try:
            if what in self.locator_dictionary.keys():
                try:
                    element = WebDriverWait(self.driver, self.timeout).until(
                        EC.presence_of_element_located(self.locator_dictionary[what])
                    )
                except(TimeoutException,StaleElementReferenceException):
                    print(self.locator_dictionary[what])
                    traceback.print_exc()
 
                try:
                    element = WebDriverWait(self.driver, self.timeout).until(
                        EC.visibility_of_element_located(self.locator_dictionary[what])
                    )
                except(TimeoutException,StaleElementReferenceException):
                    print(self.locator_dictionary[what])
                    traceback.print_exc()

                try:
                    element = WebDriverWait(self.driver, self.timeout).until(
                        EC.element_to_be_clickable(self.locator_dictionary[what])
                    )
                except(TimeoutException,StaleElementReferenceException):
                    print(self.locator_dictionary[what])
                    traceback.print_exc()
                # I could have returned element, however because of lazy loading, I am seeking the element before return
                print("[+] Found Elemnet: {}".format(self.locator_dictionary[what]))
                return self.find_element(*self.locator_dictionary[what])
        except AttributeError:
            super(BasePage, self).__getattribute__("method_missing")(what)
 
    def method_missing(self, what):
        print("No {} here!".format(what))

    def find_element(self, *loc):
        try:
            element = WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located(loc)
            )
            self.elem_status = True
        except(TimeoutException,StaleElementReferenceException):
            print(loc)
            self.elem_status = False
            traceback.print_exc()

        try:
            element = WebDriverWait(self.driver, self.timeout).until(
                EC.visibility_of_element_located(loc)
            )
            self.elem_status = True
        except(TimeoutException,StaleElementReferenceException):
            print(loc)
            self.elem_status = False
            traceback.print_exc()

        try:
            element = WebDriverWait(self.driver, self.timeout).until(
                EC.element_to_be_clickable(loc)
            )
        except(TimeoutException,StaleElementReferenceException):
            print(loc)
            traceback.print_exc()

        print("[+] Found Elemnet: {}".format(loc))
        return element
        
    def hover(self, element):
        ActionChains(self.broswer.move_to_element(element).perform())
        # I don't like this but hover is sensitive and needs some sleep time.
        time.sleep(5)
    
    def open(self, url):
        self.driver.get(url)

    def click_on(self, element, attempts=2):
        while attempts > 0:
            try:
                element.click()
                return
            except ElementClickInterceptedException:
                attempts -= 1
                print("[-] Click Intercepted, trying {} more times".format(attempts))
            time.sleep(2)
        
    def wait_is_clickable(self, *loc):
        element = WebDriverWait(self.driver, self.timeout).until(
            EC.element_to_be_clickable(*loc)
        )
        return element


    def fill_text_field(self, field_elem, value):
        """
        desc: clears and writes to a text field.
        param: field_elem (Webelement) text field in which to write.
        param: value (String) text to be entered into the field.
        """
        try:
            field_elem.location_once_scrolled_into_view
            field_elem.clear()
            field_elem.send_keys(value)
            print("Entered field value: {}".format(value))
        except Exception as e:
            print(e)

    @staticmethod
    def csv_to_list_of_dicts(input_file):
        testlist = []
        with open(input_file, "r") as infile:
            reader = csv.reader(infile)
            for row in reader:
                testlist.append(row)

        dictlist = []
        for row in range(1, len(testlist)):
            testdict = {}
            for item in range(len(testlist[row])):
                key = testlist[0][item]
                value = testlist[row][item]
                testdict[key] = value
            dictlist.append(testdict)

        return dictlist
        