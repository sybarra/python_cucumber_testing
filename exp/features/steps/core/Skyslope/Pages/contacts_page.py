from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from skyslope.pages.skyslope_base import BasePage

import time

class Contacts(BasePage):
    def __init__(self, selenium_driver):
        BasePage.__init__(self, selenium_driver)

    locator_dictionary = {
        #Buttons
        "next": (By.LINK_TEXT, "Next"),
        "cancel": (By.LINK_TEXT, "Cancel"),
        "save1": (By.LINK_TEXT, "Save"),
        "submit": (By.LINK_TEXT, "Submit")
    }
    
    def next_page(self):
        """
        desc: Saves the current page and navigates to the next page.
        """
        print("[+] Saving current page and click button to navigate to the next page.")
        save_button = self.wait_is_clickable(self.locator_dictionary["save1"])
        self.click_on(save_button)
        next_button = self.wait_is_clickable(self.locator_dictionary["next"])
        self.click_on(next_button, attempts=30)

    def save_and_submit(self):
        """
        desc: Click the save then the Submit button.
        """
        print("Saving the current page and submitting.")
        save_button = self.wait_is_clickable(self.locator_dictionary["save1"])
        self.click_on(save_button)
        next_button = self.wait_is_clickable(self.locator_dictionary["submit"])
        self.click_on(next_button, attempts=30)
    

    class Seller(BasePage):
        def __init__(self, selenium_driver):
            BasePage.__init__(self, selenium_driver)

        locator_dictionary = {
            #Fields
            "page_id": (By.XPATH, "//li[@class='active']/a[span='Seller / Landlord']"),
            "First_Name": (By.XPATH, "//div[label=\"Seller's First Name\"]/input"),
            "Last_Name": (By.XPATH, "//div[label=\"Seller's Last Name\"]/input")
        }
    
        def assert_page(self):
            assert self.find_element(*self.locator_dictionary["page_id"])

        def fill_seller_data(self, firstname="Sammy", lastname="Seller"):
            self.assert_page()
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["First_Name"]),
                firstname)
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Last_Name"]),
                lastname)
 
     
    class Buyer(BasePage):
        def __init__(self, selenium_driver):
            BasePage.__init__(self, selenium_driver)

        locator_dictionary = {
            "page_id": (By.XPATH, "//li[@class='active']/a[span='Purchaser / Tenant']"),
            "First_Name": (By.XPATH, "//div[label=\"Buyer's First Name\"]/input"),
            "Last_Name": (By.XPATH, "//div[label=\"Buyer's Last Name\"]/input"),
            "Phone": (By.ID, "ContentPlaceHolder1_purchaser1_TxtPhone")
        }

        def assert_page(self):
            self.wait_is_clickable(self.locator_dictionary["First_Name"])

        def fill_buyer_data(self, firstname="Betty", lastname="Buyer", phone="919-434-1234"):
            self.assert_page()
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["First_Name"]),
                firstname)
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Last_Name"]),
                lastname)
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Phone"]),
                phone)


    class Title(BasePage):
        def __init__(self, selenium_driver):
            BasePage.__init__(self, selenium_driver)

        locator_dictionary = {
            "Closing_Officer_FN": (By.ID, 
                        "ContentPlaceHolder1_title_TxtFirstName"),
            "Closing_Officer_LN": (By.ID, 
                        "ContentPlaceHolder1_title_TxtLastName"),
            "Phone": (By.ID, 
                        "ContentPlaceHolder1_title_TxtPhone"),
            "Title_Company": (By.ID, 
                        "ContentPlaceHolder1_title_TxtCompany"),
            "Email": (By.ID, 
                        "ContentPlaceHolder1_title_TxtEmail")
        }

        def fill_title_data(self, closing_officer_fn="Clemet", 
                closing_officer_ln="Closer", phone="919-434-4321", 
                titleco="TitleCo", email="clemet.closer@titleco.com"):

            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Closing_Officer_FN"]),
                closing_officer_fn)
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Closing_Officer_LN"]),
                closing_officer_ln)
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Phone"]),
                phone)
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Title_Company"]),
                titleco)
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Email"]),
                email)


    class Agent(BasePage):
        def __init__(self, selenium_driver):
            BasePage.__init__(self, selenium_driver)

        locator_dictionary = {
            "First_Name": (By.ID,
                        "ContentPlaceHolder1_AgentOtherSide_TxtFirstName"),
            "Last_Name": (By.ID,
                        "ContentPlaceHolder1_AgentOtherSide_TxtLastName"),
            "Phone": (By.ID,
                        "ContentPlaceHolder1_AgentOtherSide_TxtPhone"),
            "Co-Broker_Company": (By.ID, 
                        "ContentPlaceHolder1_AgentOtherSide_TxtCompany")
        }

        def fill_otherside_agent(self, first_name="Agnis", last_name="Agent",
                phone="919-434-1122", broker_company="BrokerCo"):
            
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["First_Name"]),
                first_name)
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Last_Name"]),
                last_name)
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Phone"]),
                phone)
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Co-Broker_Company"]),
                broker_company)

    class Lender(BasePage):
        def __init__(self, selenium_driver):
            BasePage.__init__(self, selenium_driver)

        locator_dictionary = {
            "First_Name": (By.ID, "ContentPlaceHolder1_Lender_TxtFirstName"),
            "Last_Name": (By.ID, "ContentPlaceHolder1_Lender_TxtLastName"),
            "Lender_Company": (By.ID, "ContentPlaceHolder1_Lender_TxtCompany")

        }

        def fill_lender_data(self, first_name="Lenny", last_name="Lender",
                            lender_company="LenderCo"):
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["First_Name"]),
                first_name
            )
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Last_Name"]),
                last_name
            )
            self.fill_text_field(
                self.find_element(*self.locator_dictionary["Lender_Company"]),
                lender_company
            )





# Steps
# def next_page(selenium_driver):
#     """
#     desc: Saves the current page and navigates to the next.
#     """
#     contacts = Contacts(selenium_driver)
#     #Submit form
#     contacts.save1.click()
#     time.sleep(3)
#     contacts.next.click()

# def fill_seller_data(selenium_driver, firstname="Sammy", lastname="Seller"):
#     seller_contact = Contacts.Seller(selenium_driver)
#     buyer_contact = Contacts.Buyer(selenium_driver)
#     seller_contact.fill_text_field(seller_contact.First_Name, firstname)
#     seller_contact.fill_text_field(seller_contact.Last_Name, lastname)
    