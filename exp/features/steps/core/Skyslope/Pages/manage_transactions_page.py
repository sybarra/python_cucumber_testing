# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from skyslope.pages.skyslope_base import BasePage

class ManageTransactions(BasePage):
    def __init__(self, selenium_driver):
        BasePage.__init__(self, selenium_driver)
        
    locator_dictionary = {
        "create_transaction": (By.XPATH, "//button[text()='Create Transaction']"),
    }
