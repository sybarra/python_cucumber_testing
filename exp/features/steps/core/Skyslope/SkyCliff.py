# -*- coding: utf-8 -*-
"""
Created on Fri Feb  7 01:01:14 2020

@author: sybarra
"""

import unittest, time
from datetime import timedelta, date
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import StaleElementReferenceException

class BaseClass(unittest.TestCase):

    #locators
    loginButton = "//ul[@id='top-menu']/li/a[text()='Login']"
    signInButton = "//button[text()='Sign in']"
    usernameField = "//div[label='Username']/div/input"
    passwordField = "//div[label='Password']/div/input"
 
    #variables
    username = "datafeedtest@skyslope.com"
    password = "N0M0reLockouts"
    signInPageTitle = "SkySlope - Customer Secure Login Page"
    officeLeadStatus = False

    def setUp(self):
        #chrome_driver = "../chromedriver-Windows"
        chrome_driver = "../chromedriver.exe"
        self.driver = webdriver.Chrome(chrome_driver)
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)


    def login(self):
        driver = self.driver

        #handle landing page
        driver.get("https://skyslope.com/")
        self.assertIn("SkySlope", driver.title)
        self.click_on(self.loginButton)

        #login
        time.sleep(3)
        usernameField = driver.find_element_by_id("username")
        usernameField.clear()
        usernameField.send_keys(self.username)

        passowordField = driver.find_element_by_id("password")
        passowordField.clear()
        passowordField.send_keys(self.password)
    
        #submit = driver.find_element_by_xpath(self.signInButton).click()
        self.click_on(self.signInButton)

    # Generics

    def fill_generic_text_field(self, label, text):
        print("[i] :: Filling Field: {0}".format(label))
        xpath = "//div[label='{0}']//input".format(label)
        field = self.driver.find_element_by_xpath(xpath)
        field.location_once_scrolled_into_view
        field.click()
        field.clear()
        field.send_keys(text)


    def fill_generic_drop_down(self, label, prefill, value):
        print("[i] :: Filling Field: {0} with {1}".format(label, value))
        xpath = "//div[label='{0}']//span[text()='{1}']".format(label, prefill)
        self.wait_is_clickable(xpath)
        field = self.driver.find_element_by_xpath(xpath)
        field.location_once_scrolled_into_view
        field.click()
        print(xpath)
        
        ddXpath = "//span[contains(@class, 'dropdown')]/input"
        dropDown = self.driver.find_element_by_xpath(ddXpath)
        dropDown.click()
        dropDown.send_keys(value)
        print(ddXpath)

        fieldSelection = "//li[text()='{0}']".format(value)
        self.driver.find_element_by_xpath(fieldSelection).click()


    def fill_office_lead(self, label, prefill, value):
        try:
            print("[i] :: Filling Field: {0} with {1}".format(label, value))
            xpath = "//div[label='{0}']//select/option[text()='{1}']".format(label, prefill)
            print(xpath)
            self.wait_is_clickable(xpath)
            field = self.driver.find_element_by_xpath(xpath)
            field.location_once_scrolled_into_view
            field.click()

            xpath = "//div[label='{0}']//select/option[text()='{1}']".format(label, value)
            print(xpath)
            self.wait_is_clickable(xpath)
            field = self.driver.find_element_by_xpath(xpath)
            field.click()
            self.officeLeadStatus = True
        except StaleElementReferenceException as StaleElement:
            print("[-] Failed to set Office Lead.")
            
        
        time.sleep(5)


    def wait_is_visible(self, xpath):
        wb = self.driver
        element = WebDriverWait(wb, 10).until(
            EC.visibility_of((By.XPATH, xpath))
        )


    def wait_is_clickable(self, xpath):
        wb = self.driver
        elem_to_click = WebDriverWait(wb, 10).until(
            EC.element_to_be_clickable((By.XPATH, xpath))
        )
    

    def today_plus(self, numDays):
        plusDate = date.today() + timedelta(days=numDays)
        return plusDate.strftime("%m/%d/%y")

    def click_on(self, xpath):
        elem = self.driver.find_element_by_xpath(xpath)
        elem.location_once_scrolled_into_view
        self.wait_is_clickable(xpath)
        elem.click()

    # Main Page

    def selectManageTransactionsTile(self):
        xpath = "//a[contains(@href, 'ManageTransactions')]"
        mgmtTrx = self.driver.find_element_by_xpath(xpath)
        mgmtTrx.click()
        # TODO: Add assertion for header


    # Transaction Form
    def click_Create_Transactions_button(self):
        xpath = "//button[text()='Create Transaction']"
        createTransactionButton = self.driver.find_element_by_xpath(xpath)
        createTransactionButton.click()


    def dismiss_dialog(self):
        xpath = "//div[h3='Search for Address']/button[@class='close']"
        self.wait_is_clickable(xpath)
        closeButton = self.driver.find_element_by_xpath(xpath)
        closeButton.click()

    def clickNext(self):
        xpath = "//a[text()='Next']"
        button = self.driver.find_element_by_xpath(xpath)
        button.location_once_scrolled_into_view
        self.wait_is_clickable(xpath)
        button.click()
    
    def clickSave(self):
        xpath = "//a[text()='Save']"
        button = self.driver.find_element_by_xpath(xpath)
        button.location_once_scrolled_into_view
        self.wait_is_clickable(xpath)
        button.click()


    def test_search_in_python_org(self):
        self.login()
        self.selectManageTransactionsTile()
        self.click_Create_Transactions_button()
        self.dismiss_dialog()
        self.fill_generic_drop_down("Agent", "Click to Search", "Sky Test")
        self.fill_generic_text_field("MLS#", "1234")
        self.fill_generic_drop_down("Checklist Type", "Select", "Traditional Sale")
        self.fill_generic_text_field("Street No.", "1")
        self.fill_generic_text_field("Street Name", "1234")
        self.fill_generic_text_field("Unit #", "1234")
        self.fill_generic_text_field("Zip Code", "1234")
        self.fill_generic_text_field("State", "AZ")
        self.fill_generic_text_field("City", "Sedona")
        self.fill_generic_drop_down("Type (Representation)", "Select", "Purchase / Tenant")
        self.fill_generic_text_field("Sale Price", "12,500")
        self.fill_generic_text_field("Year Built", "2005")
        self.fill_generic_drop_down("Source", "Select", "Sign")
        self.fill_generic_text_field("File ID", "1234")
        while not self.officeLeadStatus:
            self.fill_office_lead("Office Lead?", "Select", "No")
        self.fill_generic_text_field("Closing Date", self.today_plus(10))
        self.fill_generic_text_field("Acceptance Date", self.today_plus(0))
        self.clickNext()

        self.fill_generic_text_field("Seller's First Name", "Sally")
        self.fill_generic_text_field("Seller's Last Name", "Seller")
        self.clickSave
        self.clickNext
        





        time.sleep(8)
        
        #elem.send_keys("pycon")
        #elem.send_keys(Keys.RETURN)
        #assert "No results found." not in driver.page_source


    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()