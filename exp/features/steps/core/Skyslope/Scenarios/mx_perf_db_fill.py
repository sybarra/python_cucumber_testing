# -*- coding: utf-8 -*-
import unittest, time
from selenium import webdriver
from skyslope.pages.skyslope_base import BasePage
from skyslope.pages.top_menu import TopMenu
from skyslope.pages.landing_page import LandingPage
from skyslope.pages.login_page import LoginPage
from skyslope.pages.dashboard_page import Dashboard
from skyslope.pages.manage_transactions_page import ManageTransactions
from skyslope.pages.create_trx_main import CreateTrxMain
from skyslope.pages.contacts_page import Contacts
from skyslope.pages.commission_info_page import CommissionInfo

from selenium.common.exceptions import ElementClickInterceptedException

class AddTransaction(object):
    
    #variables
    username = "datafeedtest@skyslope.com"
    password = "N0M0reLockouts"
    
    def __init__(self, prime_agent="Sky Test", street_num="1", street_name="MxAssure Perf St.", sale_price="120000.00"):
        chrome_driver = "../chromedriver.exe"
        self.driver = webdriver.Chrome(chrome_driver)
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.driver.get("https://skyslope.com")

        #Form values
        self.prime_agent = prime_agent
        self.street_num = street_num
        self.street_name = street_name
        self.sale_price = sale_price
        
        #create page objects
        self.top_menu = TopMenu(self.driver)
        self.landing_page = LandingPage(self.driver)
        self.login_page = LoginPage(self.driver)
        self.dashboard_page = Dashboard(self.driver)
        self.manage_trx_page = ManageTransactions(self.driver)
        self.create_trx_page = CreateTrxMain(self.driver)
        self.contacts_page = Contacts(self.driver)
        self.seller_contact_page = Contacts.Seller(self.driver)
        self.buyer_contact_page = Contacts.Buyer(self.driver)
        self.title_contact_page = Contacts.Title(self.driver)
        self.commission_info_page = CommissionInfo(self.driver)
        
    def __del__(self):
        print("[i] Closing Webdriver instance")
        self.driver.quit()
        time.sleep(1)

    def login(self):
        #self.assertIn("SkySlope", self.driver.title)
        self.landing_page.login_button.click()
        self.login_page.login("datafeedtest@skyslope.com", "N0M0reLockouts")
        
    def navigate_to_create_page(self):
        self.dashboard_page.manage_transactions.click()
        self.manage_trx_page.create_transaction.click()
        
    def add_trx(self):
        try:
            self.navigate_to_create_page()
            self.create_trx_page.dismiss_addr_dialog()
            self.create_trx_page.fill_form(
                agent=self.prime_agent,
                street_num=self.street_num,
                street_name=self.street_name,
                sale_price=self.sale_price
            )
            self.create_trx_page.next.click()
            self.seller_contact_page.fill_seller_data()
            self.contacts_page.next_page()
            self.buyer_contact_page.fill_buyer_data()
            self.contacts_page.next_page()
            self.title_contact_page.fill_title_data()
            self.contacts_page.next_page()
            Contacts.Agent(self.driver).fill_otherside_agent()
            self.contacts_page.next_page()
            Contacts.Lender(self.driver).fill_lender_data()
            self.contacts_page.save_and_submit()
            self.commission_info_page.fill_commission_info()
            self.commission_info_page.submit.click()
            time.sleep(5)
            self.top_menu.skyslope_logout()
            self.driver.quit()
            print("[+] INFO :: Added Transaction: {0} {1}".format(self.street_num, self.street_name))
            return True

        except Exception as e:
            print(e)
            self.top_menu.skyslope_logout()
            print("[-] Error :: Failed to add Transaction: {0} {1}".format(
                self.street_num, self.street_name))
            self.driver.quit()
            return False    


        #TODO: Delete after development is complete
        time.sleep(2)
         
        
if __name__=='__main__':
    #agents_list = ["App Team", "Sky Test"]
    batch_one = ["Eden Avalos", "Gigmy Bista", "Tiffany Trimble", 
                 "Marcus Jackson", "Yemily Lopez - Pena", "Darius Dillard",
                 "Caitlyn Henderson", "Efrain Reyes", "Nadia Zaman",
                 "Susan Resendiz", "Jannes Hendriks", "Josh Giordani-Enlow"]
    agents_list = ["Yemily Lopez - Pena"]
    fail_list = []
    for agent in agents_list:
        for num in range(1, 21):
            filler = AddTransaction(prime_agent=agent, street_num=num, street_name="MxAssure {} St.".format(agent))
            filler.landing_page.login_button.click()
            filler.login_page.login("datafeedtest@skyslope.com", "N0M0reLockouts")
            if not filler.add_trx():
                fail_list.append("{}: {} {}".format(
                    agent, filler.street_num, filler.street_name))
    print("The following records failed to be added:")
    print(fail_list)
            

    

    
