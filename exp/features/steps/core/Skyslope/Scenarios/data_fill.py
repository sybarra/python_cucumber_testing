# -*- coding: utf-8 -*-
import unittest, time
from selenium import webdriver
from Skyslope.Pages.skyslope_base import BasePage
from Skyslope.Pages.top_menu import TopMenu
from Skyslope.Pages.landing_page import LandingPage
from Skyslope.Pages.login_page import LoginPage
from Skyslope.Pages.dashboard_page import Dashboard
from Skyslope.Pages.manage_transactions_page import ManageTransactions
from Skyslope.Pages.create_trx_main import CreateTrxMain
from Skyslope.Pages.contacts_page import Contacts
from Skyslope.Pages.commission_info_page import CommissionInfo

class AddTransaction(object):
    
    #variables
    username = "datafeedtest@skyslope.com"
    password = "N0M0reLockouts"
    
    def __init__(self):
        chrome_driver = "../../chromedriver.exe"
        self.driver = webdriver.Chrome(chrome_driver)
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.driver.get("https://skyslope.com")
        
        #create page objects
        self.top_menu = TopMenu(self.driver)
        self.landing_page = LandingPage(self.driver)
        self.login_page = LoginPage(self.driver)
        self.dashboard_page = Dashboard(self.driver)
        self.manage_trx_page = ManageTransactions(self.driver)
        self.create_trx_page = CreateTrxMain(self.driver)
        self.contacts_page = Contacts(self.driver)
        self.seller_contact_page = Contacts.Seller(self.driver)
        self.buyer_contact_page = Contacts.Buyer(self.driver)
        self.title_contact_page = Contacts.Title(self.driver)
        self.commission_info_page = CommissionInfo(self.driver)
        
    def __del__(self):
        self.driver.close()

    def login(self):
        #self.assertIn("SkySlope", self.driver.title)
        self.landing_page.login_button.click()
        self.login_page.login("datafeedtest@skyslope.com", "N0M0reLockouts")
        
    def navigate_to_create_page(self):
        self.dashboard_page.manage_transactions.click()
        self.manage_trx_page.create_transaction.click()
        
    def add_trx(self, **kwargs):
        try:
            self.navigate_to_create_page()
            self.create_trx_page.dismiss_addr_dialog()
            self.create_trx_page.fill_form(
                agent=kwargs["Agent"],
                street_num=kwargs["Street No."],
                street_name=kwargs["Street Name"],
                zip=kwargs["Zip Value"],
                sale_price=kwargs["Sale Price"],
                source=kwargs["Source"],
                office_lead=kwargs["Office Lead Value"],
                file_id=kwargs["File ID"],
                closing_date=kwargs["Closing Date"],
                checklist_type=kwargs["Checklist Sale"],
                type_rep=kwargs["Sale type"],
                yr_built=kwargs["Year Built"],
                accp_date=kwargs["Acceptance Date"]
            )
            self.create_trx_page.next.click()
            self.seller_contact_page.fill_seller_data()
            self.contacts_page.next_page()
            self.buyer_contact_page.fill_buyer_data()
            self.contacts_page.next_page()
            self.title_contact_page.fill_title_data()
            self.contacts_page.next_page()
            Contacts.Agent(self.driver).fill_otherside_agent()
            self.contacts_page.next_page()
            Contacts.Lender(self.driver).fill_lender_data()
            self.contacts_page.save1.click()
            self.contacts_page.submit.click()
            self.commission_info_page.fill_commission_info()
            self.commission_info_page.submit.click()
            # self.top_menu.homelogo.click()
            self.top_menu.skyslope_logout()
            return True

        except Exception as e:
            print("Failed to add transaction with address: {no} {name}".format(
                no = kwargs["Street No."],
                name = kwargs["Street Name"])
            )
            self.top_menu.skyslope_logout()
            return False    


        #TODO: Delete after development is complete
        time.sleep(5)
         
        
if __name__=='__main__':
    filler = AddTransaction()
    filler.landing_page.login_button.click()

    data = BasePage.csv_to_list_of_dicts("../Data/skyslope.csv")
    for transaction in data:
        if transaction['Execution Flag'] == "Yes":
            print(transaction)
            success = False
            while not success:
                filler.login_page.login("datafeedtest@skyslope.com", "N0M0reLockouts")
                success = filler.add_trx(**transaction)
            

    

    
