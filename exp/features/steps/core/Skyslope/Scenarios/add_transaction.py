# -*- coding: utf-8 -*-
import unittest, time
from selenium import webdriver
from skyslope.pages.top_menu import TopMenu
from skyslope.pages.landing_page import LandingPage
from skyslope.pages.login_page import LoginPage
from skyslope.pages.dashboard_page import Dashboard
from skyslope.pages.manage_transactions_page import ManageTransactions
from skyslope.pages.create_trx_main import CreateTrxMain
from skyslope.pages.contacts_page import Contacts
from skyslope.pages.commission_info_page import CommissionInfo

class AddTransaction(unittest.TestCase):
    
    #variables
    username = "datafeedtest@skyslope.com"
    password = "N0M0reLockouts"
    
    def setUp(self):
        chrome_driver = "../chromedriver.exe"
        self.driver = webdriver.Chrome(chrome_driver)
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.driver.get("https://skyslope.com")
        
        #create page objects
        self.top_menu = TopMenu(self.driver)
        self.landing_page = LandingPage(self.driver)
        self.login_page = LoginPage(self.driver)
        self.dashboard_page = Dashboard(self.driver)
        self.manage_trx_page = ManageTransactions(self.driver)
        self.create_trx_page = CreateTrxMain(self.driver)
        self.contacts_page = Contacts(self.driver)
        self.seller_contact_page = Contacts.Seller(self.driver)
        self.buyer_contact_page = Contacts.Buyer(self.driver)
        self.title_contact_page = Contacts.Title(self.driver)
        self.commission_info_page = CommissionInfo(self.driver)
        
    def login(self):
        self.landing_page.login_button.click()
        self.login_page.login("datafeedtest@skyslope.com", "N0M0reLockouts")
        
    def navigate_to_create_page(self):
        self.dashboard_page.manage_transactions.click()
        self.manage_trx_page.create_transaction.click()
        
    def test_adding_transaction(self):
        self.assertIn("SkySlope", self.driver.title)
        self.login()
        self.navigate_to_create_page()
        self.create_trx_page.dismiss_addr_dialog()
        self.create_trx_page.fill_form()
        self.create_trx_page.next.click()
        self.seller_contact_page.fill_seller_data()
        self.contacts_page.next_page()
        self.buyer_contact_page.fill_buyer_data()
        self.contacts_page.next_page()
        self.title_contact_page.fill_title_data()
        self.contacts_page.next_page()
        Contacts.Agent(self.driver).fill_otherside_agent()
        self.contacts_page.next_page()
        Contacts.Lender(self.driver).fill_lender_data()
        self.contacts_page.save1.click()
        self.contacts_page.submit.click()
        self.commission_info_page.fill_commission_info()
        self.commission_info_page.submit.click()
        self.top_menu.skyslope_logout()        


        #TODO: Delete after development is complete
        time.sleep(5)
        
    def tearDown(self):
        self.driver.close()
         
        
if __name__=='__main__':
    unittest.main()
