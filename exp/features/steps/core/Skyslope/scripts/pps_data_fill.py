from Skyslope.Scenarios.data_fill import AddTransaction
from Skyslope.Pages.skyslope_base import BasePage

test_data_file = "../Data/skyslope.csv"
skyslope_uername = "datafeedtest@skyslope.com"
skyslope_password = "N0M0reLockouts"

filler = AddTransaction()
filler.landing_page.login_button.click()

data = BasePage.csv_to_list_of_dicts(test_data_file)
for transaction in data:
    if transaction['Execution Flag'] == "Yes":
        print(transaction)
        success = False
        while not success:
            filler.login_page.login(skyslope_uername, skyslope_password)
            success = filler.add_trx(**transaction)