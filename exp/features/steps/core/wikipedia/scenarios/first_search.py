from steps.core.wikipedia.pages.landing_page import LandingPage
from selenium import webdriver
import time

def open_wiki(driver):
    landing_page = LandingPage(driver)
    landing_page.driver.get(landing_page.base_url)
    #TODO: assert page title

def search(driver):
    landing_page.searchbox.sendkeys("cucumber")
    time.sleep(5)

driver = webdriver.Chrome("../../chromedriver.exe")
search(driver)