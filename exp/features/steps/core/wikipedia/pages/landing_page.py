# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from steps.core.wikipedia.pages.wiki_base import BasePage

class LandingPage(BasePage):
    def __init__(self, selenium_driver):
        BasePage.__init__(
                self,
                selenium_driver
        )
        
    locator_dictionary = {
        "header": (By.XPATH, "//h1/div/div"),
        "searchbox": (By.ID, "searchInput")
    }
