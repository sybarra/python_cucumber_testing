from steps.core.wikipedia.scenarios.first_search import open_wiki 

@given('I go to Wikipedia home page')
def go_to_Wikipedia_home_page(context):
    open_wiki(context.driver)

@when("I search for the term cucumber")   
def search_for_the_term_cucumber(context):
    search(context.driver)