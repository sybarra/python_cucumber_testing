from behave import *

@given('we have behave installed')
def we_have_behave_installed(context):
    pass

@when('we implement a test')
def we_implement_a_test(context):
    assert True is not False

@then('behave will test it for us!')
def behave_will_test_it_for_us(context):
    assert context.failed is False